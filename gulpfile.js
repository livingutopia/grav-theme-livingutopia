var gulp = require('gulp');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var csscomb = require('gulp-csscomb');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var argv = require('yargs').argv;
const path = require('path')
function resolvePath(filepath) {
  if (filepath[0] === '~') {
      return path.join(process.env.HOME, filepath.slice(1));
  } else {
    return path.resolve(filepath);
  }
}

// configure the paths
var watch_dir = './scss/**/*.scss';
var src_dir = './scss/*.scss';
var dest_dir = './css-compiled';

var paths = {
    source: src_dir
};


gulp.task('watch', function() {
  gulp.watch(watch_dir, ['build']);
});

gulp.task('build', function() {
  const sassIncludePaths = [
    './scss/',
  ]
  if(argv['quark-dir']) {
    sassIncludePaths.push(path.join(argv['quark-dir'], 'scss'))
  } else {
    sassIncludePaths.push('../quark/scss')
    sassIncludePaths.push('../grav-theme-quark/scss')
  }

  gulp.src(paths.source)
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compact',
      precision: 10,
      includePaths: sassIncludePaths.map(resolvePath)
    })
      .on('error', sass.logError)
    )
    .pipe(sourcemaps.write())
    .pipe(autoprefixer())       
    .pipe(gulp.dest(dest_dir))
    .pipe(csscomb())
    .pipe(cleancss())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(dest_dir));
});

gulp.task('default', ['build']);
