# living utopia Grav Theme
- based on [Quark](https://github.com/getgrav/grav-theme-quark/)


### Supported Page Templates

* Default: `default.md`
* Error `error.md`
* Blog: `blog.md`
* Blog item: `item.md`
* Modular: `modular.md`
  * Hero (full width image) `hero.md`
  * Brands (List of links with images)  `brands.md`
  * List of childrens: `childrenlist.md`
  * Features: `features.md`
  * Image Gallery: `gallery.md`
  * Schedule: `schedule.md`
  * Team: `team.md`
  * Text: `text.md`

